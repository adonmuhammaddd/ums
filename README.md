<p align="center"><a href="#" target="_blank"><img src="https://abstractentropy.com/content/images/2020/08/ln.svg" width="400"></a></p>

## About UMS

User Management System using Laravel + Vue.js

## Installation

- Database : **ums**
- run **composer install**
- run **npm install && npm run dev**
- run **php artisan migrate:refresh --seed**
- run **php artisan optimize**

## Run

- run **php artisan serve**

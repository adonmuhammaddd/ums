<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JamesDordoy\LaravelVueDatatable\Http\Resources\DataTableCollectionResource;
use App\Models\User;

class AuthController extends Controller
{
    public function index(Request $request)
    {
        // $length = $request->input('length');
        // $orderBy = $request->input('column');
        // $orderByDir = $request->input('dir', 'asc');
        // $searchValue = $request->input('search');
        
        // $query = User::eloquentQuery($orderBy, $orderByDir, $searchValue);
        // $data = $query->paginate($length);
        
        // return new DataTableCollectionResource($data);

        $users = User::orderBy('created_at', 'DESC')->get();
        return response()->json($users);
    }

    public function show($id)
    {
        $user = User::find($id);
        return response()->json($user);
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validation->fails()) :
            return response()->json([
                'status' => false,
                'message' => $validation->errors()
            ], 403);
        endif;

        $newUser = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json([
            'status' => true,
            'message' => 'Data has been saved',
            'data' => $newUser
        ]);
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        if ($validation->fails()) :
            return response()->json([
                'status' => false,
                'message' => $validation->errors()
            ], 403);
        endif;

        $updateUser = User::where('id', $id)
                            ->update([
                                'name' => $request->name,
                                'email' => $request->email
        ]);

        return response()->json([
            'status' => true,
            'message' => 'Data has been updated',
            'data' => $updateUser
        ]);
    }

    public function destroy($id)
    {
        $userDelete = User::find($id);
        $userDelete->delete();

        return response()->json([
            'status' => true,
            'message' => 'Data has been deleted'
        ]);
    }

}

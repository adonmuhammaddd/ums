<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class UserController extends Controller
{
    public function upload(Request $request, $id)
    {
        $user = User::find($id);
        $imageName = time().'.'.$request->image->extension();

        $path = public_path('images/user');

        if (!empty($user->image) && file_exists($path.'/'.$user->image)) :
            unlink($path.'/'.$user->image);
        endif;

        $user->image = $imageName;
        $user->save();

        $request->image->move(public_path('images/user'), $imageName);

        return response()->json([
            'status' => true,
            'message' => 'Image has been uploaded',
            'data' => $user
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    
    public function run()
    {
        for( $i = 1; $i <= 50; $i++) :
            $faker = Faker::create();
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => Hash::make('passw00rd')
            ]);
        endfor;
    }
}

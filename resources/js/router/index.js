import Vue from 'vue'

import VueRouter from 'vue-router'
Vue.use(VueRouter)

const Home = require('../pages/Home.vue').default
const About = require('../pages/About.vue').default
// const NotFound = require('../pages/NotFound.vue').default  // <------------ Bisa ini
import NotFound from '../pages/NotFound.vue' // <---------- Atau ini (Karna nama key dan valuenya sama)
import User from '../pages/User.vue'
import Profile from '../pages/Profile.vue'
import Register from '../pages/Register.vue'
import EditUser from '../pages/EditUser.vue'

const routes = [
    {
        name: 'Home',
        path: '/',
        component: Home
    },
    {
        name: 'Home',
        path: '/home',
        component: Home
    },
    {
        name: 'About',
        path: '/about',
        component: About
    },
    {
        name: 'User',
        path: '/users',
        component: User
    },
    {
        name: 'Register',
        path: '/user/create',
        component: Register
    },
    {
        name: 'Profile',
        path: '/user/:id',
        component: Profile,
        props: true
    },
    {
        name: 'EditUser',
        path: '/user/:id/edit',
        component: EditUser,
        props: true
    },
    {
        path: '*',
        component: NotFound
    }
]

const router = new VueRouter ({
    mode: 'history',
    linkActiveClass: 'active',
    // routes: routes // <------------ Bisa ini
    routes // <---------- Atau ini (Karna nama key dan valuenya sama)
})

export default router
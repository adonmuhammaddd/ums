require('./bootstrap');

import Vue from 'vue'

import VueNoty from 'vuejs-noty'
Vue.use(VueNoty)
import 'vuejs-noty/dist/vuejs-noty.css'

import DataTable from 'laravel-vue-datatable';
Vue.use(DataTable);


import router from './router' // <-------------- ../router/index.js

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('sidebar-component', require('./components/Sidebar.vue').default);
Vue.component('top-navbar-component', require('./components/TopNavbar.vue').default);
Vue.component('footer-component', require('./components/Footer.vue').default);

const app = new Vue({
    el: '#app',
    data: {
        title: 'Laravel-Vue'
    },
    router
});

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('sb-admin/css/sb-admin-2.min.css') }}">
    <link href="{{ asset('sb-admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">

</head>
<body id="page-top">
        <div id="app">
            {{-- <h1>Hello, @{{ title }}</h1> --}}
            {{-- <h1 v-text="'Hello, ' + title"></h1> --}}
            <div id="wrapper">
                <sidebar-component></sidebar-component>
                
                <div id="content-wrapper" class="d-flex flex-column">
                    <div id="content">
                        <top-navbar-component></top-navbar-component>
                            <div class="container-fluid">
                                <router-view></router-view>
                            </div>
                    </div>
                    <footer-component></footer-component>
                </div>
        
            </div>
        </div>
    
</body>
<script src="{{ asset('sb-admin/vendor/jquery/jquery.min.js')}} "></script>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }} "></script>
<script src="{{ asset('sb-admin/js/sb-admin-2.min.js') }}"></script>
{{-- <script src="{{ asset('sb-admin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sb-admin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script> --}}
</html>
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', [AuthController::class, 'index']);
Route::post('/users', [AuthController::class, 'store']);

Route::get('/user/{id}', [AuthController::class, 'show']);

Route::delete('/user/{id}', [AuthController::class, 'destroy']);
Route::put('/user/{id}', [AuthController::class, 'update']);
Route::post('/user/upload-image/{id}', [UserController::class, 'upload']);